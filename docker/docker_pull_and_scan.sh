#!/bin/bash
set -x

ZAP_PATH=/home//Downloads/ZAP_2.7.0/zap.sh
ZAP_API_KEY=
ZAP_PORT=8080

export ZAP_PATH
export ZAP_API_KEY
export ZAP_PORT



app_name=$1
ver=$2


#if sudo docker ps | grep ${app_name};
#then
#
#   sudo docker container stop 
#fi



sudo docker pull turnkeylinux/${app_name}-${ver}
sudo bash docker_base.sh ${app_name}
id=$(sudo docker ps | grep ${app_name} | awk '{print $1}')
ip=$(sudo docker inspect ${id} | jq -r .[].NetworkSettings.IPAddress)
ports=$(sudo docker inspect ${id} | jq -r .[].NetworkSettings.Ports | grep -o '[0-9]*')

zap-cli scanners enable

#$ZAP_PATH -session "/home//Fuzz Session.session"

for port in ${ports};
do
    zap-cli open-url http://${ip}:${port}
    sleep 10
    zap-cli quick-scan --spider --recursive http://${ip}:${port} &
    sleep 10
    zap-cli quick-scan --ajax-spider --recursive http://${ip}:${port} &
    sleep 10
    zap-cli active-scan --recursive http://${ip}:${port} &
    sleep 10
    wait
done


