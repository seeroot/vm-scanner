#!/bin/bash

app_name=$1

mkdir /root/${app_name}

cat > /root/${app_name}/inithooks.conf <<EOF
export ROOT_PASS=secretrootpass
export DB_PASS=secretmysqlpass
export APP_PASS=secretadminwppass
export APP_EMAIL=admin@example.com
export APP_DOMAIN=www.example.com
export HUB_APIKEY=SKIP
export SEC_UPDATES=FORCE
EOF

cat > /root/${app_name}/Dockerfile <<EOF
FROM turnkeylinux/${app_name}-14.2
ADD inithooks.conf /etc/inithooks.conf
EOF

docker build -t ${app_name}-14.2 /root/${app_name}
docker run -i -t -d ${app_name}-14.2
