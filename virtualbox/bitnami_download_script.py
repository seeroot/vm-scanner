#!/usr/bin/python3
import os
import urllib.request
import subprocess
from bs4 import BeautifulSoup
import requests

#hardcode an username here
username = ""
home = "/home/" + username

dl_path = home + "/fuzz-targets"

if not os.path.exists(dl_path):
    print("path doesn't exist. trying to make")
    os.makedirs(dl_path)

os.chdir(dl_path)

# Page with all the different vms
url = "https://bitnami.com/stacks/virtual-machine"

request = requests.get(url)

data = request.text

soup = BeautifulSoup(data, "lxml")

# Find all links
links = soup.find_all("a")

l = []

# Create a list of all links that are vm pages
l = [str(i.attrs["href"]) for i in links if "/virtual-machine" in i.attrs["href"] and "/stack/" in i.attrs["href"]]

uris = list(set(l))

# Go through all the pages and find vm download links
download_links = []
for uri in uris:
    app_page = "https://bitnami.com" + uri
    r = requests.get(app_page)
    data = r.text
    soup = BeautifulSoup(data, "lxml")
    dlink = soup.findAll("a", {"class": "direct_download_link button button-action"})
    for i in dlink:
        if ".ova" in i.attrs["href"]:
            download_links.append(i.attrs["href"])

# Unique the list of vm downloads
vm_links = list(set(download_links))

# Limit amount of downloads a day
count = 0
threshold = 7

# Download each vm
for vm in vm_links:
    vm_name = vm.split("/")[4].split("-")[1]
    file_name = vm.split("/")[4]
    download_url = "https://bitnami.com/" + vm + "?with_popup_skip_signin=1"
    if os.path.isfile(file_name) is False: 
        print("Downloading " + vm_name + "........")
        urllib.request.urlretrieve(download_url, file_name)
        print("Downloaded " + vm_name)
        count += 1
    else:
        continue
    p1 = subprocess.Popen(["vboxmanage", "import", dl_path + "/" + file_name], universal_newlines=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    p1.wait()
    p1_output, p1_err = p1.communicate()
    print(p1_output)
    vbox_name = [line for line in p1_output.splitlines() if "Suggested VM name" in line][0].split()[4].split('"')[1]
    subprocess.run(["VBoxManage", "startvm", "--type", "headless", vbox_name])
    if threshold == count:
        break

# Add a check for whether the file exists and whether it is smaller than download