#!/usr/bin/python3
import os
import urllib.request
import subprocess
import requests
from bs4 import BeautifulSoup

# harcode a username here
username = ""
home = "/home/" + username

dl_path = home + "/fuzz-targets"

if not os.path.exists(dl_path):
    print("path doesn't exist. trying to make")
    os.makedirs(dl_path)

os.chdir(dl_path)

# Page with all the different vms
urls = ["https://www.turnkeylinux.org/front?page=1", "https://www.turnkeylinux.org/front?page=2", "https://www.turnkeylinux.org/front?page=3", "https://www.turnkeylinux.org/front?page=4", "https://www.turnkeylinux.org/front?page=5"]

links = []
uris = []

for url in urls:
    r = requests.get(url)
    data = r.text
    soup = BeautifulSoup(data, "lxml")
    data = soup.findAll('div', attrs={'class':"field-content"})

    for div in data:
        links.append(div.findAll('a'))


flat_list_of_links = [item for sublist in links for item in sublist]

for link in flat_list_of_links:
    if link.has_attr('href'):
        uris.append(link['href'])

vm_links = ["https://www.turnkeylinux.org" + uri for uri in uris]

links = []
uris = []
flat_list_of_links = []

for vm_link in vm_links:
    r = requests.get(vm_link)
    data = r.text
    soup = BeautifulSoup(data, "lxml")
    data = soup.findAll('div', attrs={'class':"vm"})

    for div in data:
        links.append(div.findAll('a'))

flat_list_of_links = [item for sublist in links for item in sublist]

for link in flat_list_of_links:
    if link.has_attr('href') and ".ova" in str(link['href']):
        uris.append(link['href'])


vm_down_links = ["http://mirror.turnkeylinux.org/turnkeylinux/images/ova/" + str(u.split("/download?file=")[1]) for u in uris]

# Unique the list of vm downloads
vm_links = list(set(vm_down_links))

# Limit amount of downloads a day
count = 0
threshold = 5

# Download each vm
for vm in vm_links:
    print(vm)
    vm_name = vm.split("http://mirror.turnkeylinux.org/turnkeylinux/images/ova/")[1]
    if os.path.isfile(vm_name) is False: 
        print("Downloading " + vm_name + "........")
        urllib.request.urlretrieve(vm, vm_name)
        print("Downloaded " + vm_name)
        count += 1
    else:
        continue
    p1 = subprocess.Popen(["vboxmanage", "import", dl_path + "/" + vm_name], universal_newlines=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    p1.wait()
    p1_output, p1_err = p1.communicate()
    print(p1_output)
    vbox_name = [line.split(" 1: Suggested VM name ")[1].split('"')[1] for line in p1_output.splitlines() if "Suggested VM name" in line][0]
    print(vbox_name)
    subprocess.run(["VBoxManage", "startvm", "--type", "headless", vbox_name])
    if threshold == count:
        break