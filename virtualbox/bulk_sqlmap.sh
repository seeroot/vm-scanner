#!/bin/bash

set -x

python /home/$(whoami)/Downloads/sqlmapproject-sqlmap-6d28ca1/sqlmap.py --crawl=10 --level=5 --risk=3 --batch -m sqlmaps.txt > sqlmap.log

grep "Available databases" sqlmap.log
