import requests
import sys

try:
    f = sys.argv[1]
except IndexError:
    print("Please enter a file as an argument")
    sys.exit()


http_proxy  = "http://127.0.0.1:8888"

proxyDict = {"http":http_proxy}

with open(f) as urls:
    for url in urls:
        try:
            url = url.rstrip("\n")
            r = requests.get(url, proxies=proxyDict)
            print(r.status_code)
        except Exception as e:
            print(e)
            print("oops")