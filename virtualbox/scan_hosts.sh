#!/bin/bash

set -x

#sudo apt-get install arp-scan
#sudo apt install python-pip -y
#pip install --upgrade zapcli

sudo ip -s -s neigh flush all

IP_LIST=$(sudo arp-scan --interface=enp4s0 --localnet | grep 'CADMUS COMPUTER SYSTEMS' | awk '{print $1}' | sort -u)

ZAP_PATH=/home/$(whoami)/Downloads/ZAP_2.7.0/zap.sh
ZAP_API_KEY=
ZAP_PORT=8080

export ZAP_PATH
export ZAP_API_KEY
export ZAP_PORT

zap-cli scanners enable

#$ZAP_PATH -session "/home/$(whoami)/Fuzz Session.session"

for ip in $IP_LIST;
do
    ports=$(nmap --script=http-headers ${ip} | grep -B1 "http-headers" | grep 'tcp' | grep 'open' | awk -F'/' '{print $1}')
    for port in ${ports};
    do
        zap-cli open-url http://${ip}:${port}
        sleep 10
        zap-cli quick-scan --spider --recursive http://${ip}:${port} &
        sleep 10
        #zap-cli quick-scan --ajax-spider --recursive http://${ip}:${port} &
        #sleep 10
        #zap-cli active-scan --recursive http://${ip}:${port} &
        #sleep 10
	#wait
    done
done
