Repo for an automated fuzzing project.  The Docker portion is very much a WiP and
still at a concept level.  The Virtualbox portion works great and has found some zero
days.  Virtualbox portion will have to be rewritten once I get some better hardware.
Will also have to make the script more dynamic as it was originally not meant to be
used by anyone else.

See the findings project for zero days found. (Currently one, as not all have 
been disclosed)

# Download VMs
```python3 bitnami_download_script.py ```

```python3 turnkey_linux_download_script.py```

# start scanning imported VMs
with ZAP already running, execute
```sudo bash scan_hosts.sh```

# passively crawl for vega
```python crawl_for_proxy.py```


todos

- make stuff more dynamic no hardcoding usernames, paths and cpu count for pools
- add gobuster like features to crawl for more urls (can be gobuster or can be python) or use the zap api
- possibly download the software github and import into zap
- multiprocessing/multithreading for the downloading and importing (working PoC done)
- use generators instead of some functions
- rewrite a lot of code into python
- add more tools, like burp, vega, wapiti, wfuzz
- start creating epics and ticket boards
- need to have the ajax crawler and traditional crawler run in a cycle
- rewrite all zap-cli into the python zap api
- install script, to determine OS and then use the appropiate commands to install python libraries and clone repos